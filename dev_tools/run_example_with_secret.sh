#!/bin/sh

# Load secrets
export $(cat ./secrets_tradfri.env | xargs)

# Start example
docker-compose kill && docker-compose down && docker-compose build

echo ""
echo ""

echo "Starting the example"
docker-compose run wrapper python3 -c "from example import run; run(\"$IP\", \"$KEY\")"
