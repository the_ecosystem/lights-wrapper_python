from __future__ import print_function
from pytradfri import Gateway
from pytradfri.api.libcoap_api import api_factory
from math import ceil
from threading import Thread, BoundedSemaphore
import sys

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

class Lights:

    def __init__(self, ip, key):
        self._gateway = Gateway()
        self._api = api_factory(ip, key)
        self._color_mapping = ColorMapping()
        self._dimmer_mapping = DimmerMapping()

        self._transition_time = 10
        # Transition time is in 10th of a second
        # --> 10 = 1s
        # --> 20 = 2s
        # --> ...

        self.refresh()

    def refresh(self):
        devices_command = self._gateway.get_devices()
        devices_commands = self._api(devices_command)
        devices = self._api(*devices_commands)

        lights = [dev for dev in devices if dev.has_light_control]

        self._lights = lights

    def get_all(self):
        return [self._serialize(l) for l in self._lights]

    def get_one(self, id):
        return self._serialize(self._get_light(id))

    def update_sync(self, cmd_dict):
        """
        Accept a single command in the form:

        ```
        {
            "id": 12312,
            "color": "warm"
        }
        ```

        Only 1 argument is allowed per command.
        The following command is INVALID !!
        ```
        {
            "id": 12312,
            "color": "warm",
            "dimmer": 13
        }
        ```
        """

        id = cmd_dict['id']
        light = self._get_light(id)
        cmd = self._parse_cmd(light, cmd_dict)
        self._api(cmd)

        return {'status': 'ok'}

    def _get_light(self, id):
        for l in self._lights:
            if l.id == id:
                return l

        raise RuntimeWarning(''.join(['Light not found', id]))

    def _parse_cmd(self, light, cmd_dict):
        if len(cmd_dict) != 2:
            raise RuntimeError("Can not be more that 1 command (+ id)")

        if 'color' in cmd_dict:
            color = cmd_dict['color']
            hex_color = self._color_mapping.to_hex(color)
            return light.light_control.set_hex_color(hex_color)

        elif 'dimmer' in cmd_dict:
            transition_time = 10
            dim_percentage = cmd_dict['dimmer']
            dim = self._dimmer_mapping.from_percent(dim_percentage)
            return light.light_control.set_dimmer(dim, transition_time=transition_time)

        elif 'on' in cmd_dict:
            turn_on = cmd_dict['on']
            if turn_on:
                state = 1
            else:
                state = 0
            return light.light_control.set_state(state)



    def update(self, *cmd_dicts):
        """
        Accepts a list of commands in the form:

        ```
        [
            {
                "id": 65540,
                "color": "warm",
                "dimmer": 11,
                "on": true
            },{
                "id": 65541,
                "color": "warm",
                "on": true,
                "dimmer": 11
            },

            .
            .
            .

        ]
        ```

        The commnands are sent asynchronously to the hub (limit of 6 concurrent commands)

        """
        light_commands = [Command(self._lights, cmd_dict) for cmd_dict in cmd_dicts]

        all_cmd = list()
        for light_command in light_commands:
            light_cmds = list()
            light_cmds.append(light_command.dim_cmd)
            light_cmds.append(light_command.color_cmd)
            light_cmds.append(light_command.state_cmd)
            for cmd in light_cmds:
                if cmd is not None:
                    all_cmd.append(cmd)

        max_concurent_commands = 6
        api_sema = BoundedSemaphore(value=max_concurent_commands)
        def run_cmd(api, cmd):
            with api_sema:
                api(cmd)

        threads = list()
        for cmd in all_cmd:
            threads.append(Thread(target=run_cmd, args=(self._api, cmd)))

        for thread in threads:
            thread.start()

        for thread in threads:
            thread.join()

        return {'status': 'ok'}


    def _serialize(self, light):
        return {
            'name': light.name,
            'id': light.id,
            'turned_on': self._get_state(light).state,
            'dimmer': self._dimmer_mapping.to_percent(self._get_state(light).dimmer),
            'color': self._color_mapping.from_hex(self._get_state(light).hex_color)
        }

    def _get_state(self, light):
        return light.light_control.lights[0]



class Command:
    def __init__(self, lights, cmd_dict):
        if not 'id' in cmd_dict:
            raise RuntimeError('Can not run command without id !!')

        self._color_mapping = ColorMapping()
        self._dimmer_mapping = DimmerMapping()

        self._light = self._get_light(lights, cmd_dict['id'])
        self.dim_cmd = self._parse_dim_cmd(cmd_dict)
        self.color_cmd = self._parse_color_cmd(cmd_dict)
        self.state_cmd = self._parse_state_cmd(cmd_dict)


    def _parse_dim_cmd(self, cmd_dict):
        transition_time = 10

        if not 'dimmer' in cmd_dict:
            return None

        dim_percentage = cmd_dict['dimmer']
        dim = self._dimmer_mapping.from_percent(dim_percentage)
        return self._light.light_control.set_dimmer(dim, transition_time=transition_time)

    def _parse_color_cmd(self, cmd_dict):
        if not 'color' in cmd_dict:
            return None

        color = cmd_dict['color']
        hex_color = self._color_mapping.to_hex(color)
        return self._light.light_control.set_hex_color(hex_color)

    def _parse_state_cmd(self, cmd_dict):
        if not 'on' in cmd_dict:
            return None

        turn_on = cmd_dict['on']
        if turn_on:
            state = 1
        else:
            state = 0

        return self._light.light_control.set_state(state)

    def _get_light(self, lights, id):
        for l in lights:
            if l.id == id:
                return l

        raise RuntimeWarning(''.join(['Light not found', id]))



class ColorMapping:
    def from_hex(self, hex_color):
        return {
            'f5faf6': 'cold',
            'f1e0b5': 'normal',
            'efd275': 'warm',
        }[hex_color]

    def to_hex(self, color):
        return {
            'cold': 'f5faf6',
            'normal': 'f1e0b5',
            'warm': 'efd275',
        }[color]


class DimmerMapping:
    def from_percent(self, percent):
        return ceil(percent * 2.54)

    def to_percent(self, value):
        return ceil(value / 2.54)


