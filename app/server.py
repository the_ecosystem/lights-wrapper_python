from __future__ import print_function
import os
from flask import Flask, jsonify, request
from lights import Lights
import sys

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)



### Initialise State ##############################################
ip = os.environ['IP']
key = os.environ['KEY']

lights = Lights(ip, key)


### Initialize web server ########################################
app = Flask(__name__)


## Get info
@app.route('/', methods=['GET'])
def all_lights():
    refresh_if_needed()
    return jsonify(lights.get_all())

@app.route('/<int:light_id>', methods=['GET'])
def a_light(light_id):
    refresh_if_needed()
    return jsonify(lights.get_one(light_id))

def refresh_if_needed():
    refresh = request.args.get('refresh')
    if refresh is not None:
        lights.refresh()


## Update
@app.route('/', methods=['POST'])
def update():
    cmd = request.get_json()
    if isinstance(cmd, list):
        res = exec_async_cmds(cmd)
    else:
        res = exec_sync_single_cmd(cmd)

    return jsonify(res)

def exec_async_cmds(cmds):
    return lights.update(*cmds)

def exec_sync_single_cmd(cmd):
    return lights.update_sync(cmd)


## Debug
@app.route('/debug/<int:light_id>/on', methods=['GET'])
def debug_on(light_id):
    cmd = {
        'id': light_id,
        'on': True
    }
    return jsonify(exec_cmd(cmd))

@app.route('/debug/<int:light_id>/off', methods=['GET'])
def debug_off(light_id):
    cmd = {
        'id': light_id,
        'on': False
    }
    return jsonify(exec_cmd(cmd))

@app.route('/debug/<int:light_id>/dim/<int:dim_percentage>', methods=['GET'])
def debug_dim(light_id, dim_percentage):
    cmd = {
        'id': light_id,
        'dimmer': dim_percentage
    }
    return jsonify(exec_cmd(cmd))

@app.route('/debug/<int:light_id>/color/<string:color>', methods=['GET'])
def debug_color(light_id, color):
    cmd = {
        'id': light_id,
        'color': color
    }
    return jsonify(exec_cmd(cmd))



### Start the app ################################################
if __name__ == '__main__':
    app.run(host="0.0.0.0", threaded=True)
